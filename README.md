# Getting Started

This repository contains the source code for the GNOME developer
documentation [Getting Started tutorial][getting-started].

### Contribution rules

- Whenever changing the example code, please limit yourself to one commit per
  language.
- Remember to update the [documentation repository][developer-www] as well

If you want to add a new language:

- add a new subdirectory to this repository
- always start from the GNOME Builder template
- create one commit for each "lesson" in the tutorial

### License

All the code is released under the terms of the [CC0-1.0
license](./LICENSES/CC0-1.0.txt).

[getting-started]: https://developer.gnome.org/documentation/tutorials/beginners/getting_started.html
[developer-www]: https://gitlab.gnome.org/Teams/documentation/developer-www/
