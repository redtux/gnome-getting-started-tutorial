
#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define TEXT_VIEWER_TYPE_WINDOW (text_viewer_window_get_type())

G_DECLARE_FINAL_TYPE (TextViewerWindow, text_viewer_window, TEXT_VIEWER, WINDOW, GtkApplicationWindow)

G_END_DECLS
