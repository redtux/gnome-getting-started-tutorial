
#pragma once

#include <adwaita.h>

G_BEGIN_DECLS

#define TEXT_VIEWER_TYPE_APPLICATION (text_viewer_application_get_type())

G_DECLARE_FINAL_TYPE (TextViewerApplication, text_viewer_application, TEXT_VIEWER, APPLICATION, AdwApplication)

TextViewerApplication *text_viewer_application_new (gchar *application_id,
                                                    GApplicationFlags  flags);

G_END_DECLS
